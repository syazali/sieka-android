package bps.sieka;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.app.AlarmManager;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import bps.sieka.apps.AppConfig;
import bps.sieka.apps.AppController;
import bps.sieka.fragments.DashboardFragment;
import bps.sieka.fragments.HomeFragment;
import bps.sieka.helpers.Notification_receiver;
import bps.sieka.helpers.SQLiteHandler;
import bps.sieka.helpers.SessionManager;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private ProgressDialog pDialog;
    private int itemNavId;
    private boolean errorSync;
    private TextView mTextMessage;
    private SQLiteHandler db;
    private SessionManager session;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    transaction.replace(R.id.main_layout,new HomeFragment()).commit();
                    itemNavId = R.id.navigation_home;
                    return true;
                case R.id.navigation_dashboard:
                    transaction.replace(R.id.main_layout,new DashboardFragment()).commit();
                    itemNavId = R.id.navigation_dashboard;
                    return true;
                case R.id.navigation_Logout:
                    logoutUserBtn();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }else{
            setNotification();
        }
        Log.e(TAG, "TIME : "+session.isLoggedIn());
    }
    private void logoutUser() {
        session.setLogin(false);

        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
    private void logoutUserBtn() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Logout");
        builder.setMessage("Anda yakin untuk logout dari akun ini?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                session.setLogin(false);

                db.deleteUsers();

               // Launching the login activity
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                BottomNavigationView nav = (BottomNavigationView) findViewById(R.id.navigation);
                nav.setSelectedItemId(itemNavId);
            }
        });
        AlertDialog dialog = builder.show();

    }

    @Override
    protected void onStart() {
        super.onStart();
        HashMap<String, String> user = db.getUserDetails();

        String access_token = user.get("access_token");
        String id_pegawai = user.get("id");
        retriveData(db.getDateToday(),id_pegawai,access_token);
    }
    private void retriveData(final String tanggal, final String id_pegawai, final String access_token) {
        // Tag used to cancel the request
        String tag_string_req = "req_presensis";
        Log.e(TAG, "Retrive IN: ");
        pDialog.setMessage("Retrive data ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.URL_PRESENSI+access_token+"&id_pegawai="+id_pegawai+"&tanggal="+tanggal, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Retrive Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session
                        session.setLogin(true);

                        // Now store the user in SQLite


                        JSONObject todayPresensi = jObj.getJSONObject("presensi");
                        String jamDatang = todayPresensi.getString("jam_datang");
                        String jamPulang = todayPresensi.getString("jam_pulang");
                        String tgl = db.getDateToday();
                        // String idStatus = todayPresensi.getString("id_status");
                        String idStatus = todayPresensi.getString("status");
                        String idShift= todayPresensi.getString("id_shift");

                        // Inserting row in users table
                        db.addPresensi(tgl,jamDatang,jamPulang,idStatus,idShift);


                        JSONArray dashboard_data = jObj.getJSONArray("kantor");
                        JSONObject jsonObject;
                        db.deleteDashboard();
                        for (int i = 0; i < dashboard_data.length(); i++) {
                            jsonObject = dashboard_data.getJSONObject(i);

                            String nama = jsonObject.getString("nama");
                            String jam_datang = jsonObject.getString("jam_datang");
                            String jam_pulang =  jsonObject.getString("jam_pulang");
                            String id_status = jsonObject.getString("id_status");
                            String id_shift =  jsonObject.getString("id_shift");

                            db.addDashboardData(nama,jam_datang,jam_pulang,id_status,id_shift);

                        }
                        FragmentManager frag = getSupportFragmentManager();
                        FragmentTransaction transaction = frag.beginTransaction();

                        transaction.replace(R.id.main_layout,new HomeFragment()).commit();
                        BottomNavigationView nav = (BottomNavigationView) findViewById(R.id.navigation);
                        nav.setSelectedItemId(R.id.navigation_home);

                        //Intent intent = new Intent(MainActivity.this, DashboardActivity.class);
                       // startActivity(intent);






                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //   Log.e(TAG, "Retrive Error: " + error.getMessage());
                //  Toast.makeText(getApplicationContext(),
                //         error.getMessage(), Toast.LENGTH_LONG).show();
                if (session.isLoggedIn()) {
                    Intent intent = new Intent(getApplicationContext(), ReloadActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        }) {

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void setNotification(){
        //7.25
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTimeInMillis(System.currentTimeMillis());
        calendar1.set(Calendar.HOUR_OF_DAY,7);
        calendar1.set(Calendar.MINUTE,25);
        calendar1.set(Calendar.SECOND,0);

        ArrayList<Long> alarm_list = new ArrayList<Long>();

        //7.25
        long alarm_1 = calendar1.getTimeInMillis();
        //7.55
        calendar1.add(Calendar.MINUTE,30);
        long alarm_2 = calendar1.getTimeInMillis();

        //15.05
        calendar1.add(Calendar.MINUTE,10+(7*60));
        long alarm_3 = calendar1.getTimeInMillis();

        //15.35
        calendar1.add(Calendar.MINUTE,30);
        long alarm_4 = calendar1.getTimeInMillis();

        //16.05
        calendar1.add(Calendar.MINUTE,30);
        long alarm_5 = calendar1.getTimeInMillis();

        //16.35
        calendar1.add(Calendar.MINUTE,30);
        long alarm_6 = calendar1.getTimeInMillis();

        //17.00
        calendar1.add(Calendar.MINUTE,25);
        long alarm_7 = calendar1.getTimeInMillis();

        alarm_list.add(alarm_1);
        alarm_list.add(alarm_2);
        alarm_list.add(alarm_3);
        alarm_list.add(alarm_4);
        alarm_list.add(alarm_5);
        alarm_list.add(alarm_6);
        alarm_list.add(alarm_7);

        for (int i = 0; i < alarm_list.size(); i++){
            /*Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(alarm_list.get(i));
            Log.e(TAG, "TIME : "+calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE)+":"+calendar.get(Calendar.SECOND));
*/
            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

            Intent intent = new Intent(this, Notification_receiver.class);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(this,i+1,intent,PendingIntent.FLAG_UPDATE_CURRENT);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                // Wakes up the device in Doze Mode
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC, alarm_list.get(i), pendingIntent);
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                // Wakes up the device in Idle Mode
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar1.getTimeInMillis(), pendingIntent);
            } else {
                // Old APIs
                alarmManager.set(AlarmManager.RTC, calendar1.getTimeInMillis(), pendingIntent);
            }
        }



    }

    private void syncData(final String tanggal, final String id_pegawai, final String access_token) {
        // Tag used to cancel the request
        String tag_string_req = "req_presensis";
        Log.e(TAG, "Retrive IN: ");
        pDialog.setMessage("Retrive data ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.URL_PRESENSI+access_token+"&id_pegawai="+id_pegawai+"&tanggal="+tanggal, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Retrive Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session
                        session.setLogin(true);

                        // Now store the user in SQLite


                        JSONObject todayPresensi = jObj.getJSONObject("presensi");
                        String jamDatang = todayPresensi.getString("jam_datang");
                        String jamPulang = todayPresensi.getString("jam_pulang");
                        String tgl = db.getDateToday();
                        // String idStatus = todayPresensi.getString("id_status");
                        String idStatus = todayPresensi.getString("status");
                        String idShift= todayPresensi.getString("id_shift");

                        // Inserting row in users table
                        db.addPresensi(tgl,jamDatang,jamPulang,idStatus,idShift);


                        JSONArray dashboard_data = jObj.getJSONArray("kantor");
                        JSONObject jsonObject;
                        db.deleteDashboard();
                        for (int i = 0; i < dashboard_data.length(); i++) {
                            jsonObject = dashboard_data.getJSONObject(i);

                            String nama = jsonObject.getString("nama");
                            String jam_datang = jsonObject.getString("jam_datang");
                            String jam_pulang =  jsonObject.getString("jam_pulang");
                            String id_status = jsonObject.getString("id_status");
                            String id_shift =  jsonObject.getString("id_shift");

                            db.addDashboardData(nama,jam_datang,jam_pulang,id_status,id_shift);

                        }

                    } else {
                        // Error in login. Get the error message
                        errorSync = true;
                    }
                } catch (JSONException e) {
                    // JSON error
                    errorSync = true;
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                errorSync = true;
            }
        }) {

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
