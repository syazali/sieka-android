package bps.sieka.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import android.text.format.Time;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;

import bps.sieka.MainActivity;
import bps.sieka.R;
import bps.sieka.StatusActivity;
import bps.sieka.helpers.SQLiteHandler;
import bps.sieka.helpers.SessionManager;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
    private static final String TAG = HomeFragment.class.getSimpleName();
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private Button btnUbahStatus;

    private TextView tvJamDatang;
    private TextView tvJamPulang;
    private TextView tvTanggal;
    private TextView tvNama;
    private TextView tvstatus;
    private Button btnStatusDatang;
    private Button btnStatusPulang;
    private Button btnLogout;
    private Button btnRefresh;

    private SQLiteHandler db;
    private SessionManager session;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        db = new SQLiteHandler(view.getContext().getApplicationContext());

        // session manager
        session = new SessionManager(view.getContext().getApplicationContext());



        db.getReadableDatabase();

        HashMap<String, String> user = db.getUserDetails();
        String nama = user.get("name");

        HashMap<String, String> presensi = db.getTodayPresensi();
        String date =presensi.get("tanggal");
        String arrival = presensi.get("jam_datang");


        String leave = presensi.get("jam_pulang");

        String statue = presensi.get("id_status");
        int shift = Integer.parseInt(presensi.get("id_shift"));
        String arrivalStatue = "On time";
        String leaveStatue = "On time";

        Locale id = new Locale("in", "ID");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", id);

        try{
            if(arrival.equals("null")){
                arrival="--:--:--";
                //keterangan status
                //jika libur maka on time
                if(shift!=5 && statue.equals("Hadir")){
                    arrivalStatue=" Belum absensi";
                }
            }else{
                //jika handkey maka cek terlambat atau gak
                //cek hanya jika bukan libur
                if(shift!=5 && statue.equals("Hadir")){
                    if(shift==1 || shift==3){
                        int g = dateFormat.parse(date+" "+arrival).compareTo(dateFormat.parse(date+" "+"07:30:00"));
                        if(g>=0){
                            arrivalStatue=" Terlambat";
                        }

                    }else {
                        if(dateFormat.parse(date+" "+arrival).compareTo(dateFormat.parse(date+" "+"08:00:00"))>=0){
                            arrivalStatue=" Terlambat";
                        }
                    }

                }
            }
            if(leave.equals("null")){
                leave="--:--:--";
                //keterangan status
                //jika libur maka on time
                if(shift!=5 && statue.equals("Hadir")){
                    leaveStatue=" Belum absensi";
                }
            }else{

                //jika handkey maka cek terlambat atau gak
                //cek hanya jika bukan libur
                if(shift!=5 && statue.equals("Hadir")){
                    if(shift==1){
                        if(dateFormat.parse(date+" "+"16:00:00").compareTo(dateFormat.parse(date+" "+leave))>=0){
                            leaveStatue=" Terlambat";
                        }
                    }else if(shift==2){
                        if(dateFormat.parse(date+" "+"15:00:00").compareTo(dateFormat.parse(date+" "+leave))>=0){
                            leaveStatue=" Terlambat";
                        }
                    }
                    else if(shift==3){
                        if(dateFormat.parse(date+" "+"16:30:00").compareTo(dateFormat.parse(date+" "+leave))>=0){
                            leaveStatue=" Terlambat";
                        }
                    }
                    else {
                        if(dateFormat.parse(date+" "+"15:30:00").compareTo(dateFormat.parse(date+" "+leave))>=0){
                            leaveStatue=" Terlambat";
                        }
                    }

                }
            }
        }catch (Exception ex){
            Log.e(TAG, "Error Convert Date : " + ex.toString());
        }

        tvJamDatang = (TextView) view.findViewById(R.id.tv_presensi_datang_time);
        tvJamPulang = (TextView) view.findViewById(R.id.tv_presensi_pulang_time);
        tvNama = (TextView) view.findViewById(R.id.tv_nama);
        tvstatus = (TextView) view.findViewById(R.id.tv_status);
        tvTanggal = (TextView) view.findViewById(R.id.tv_tanggal);
        btnStatusDatang = (Button) view.findViewById(R.id.button_presensi_datang_label);
        btnStatusPulang = (Button) view.findViewById(R.id.button_presensi_pulang_label);

        tvNama.setText(nama);
        tvJamDatang.setText(arrival);
        tvJamPulang.setText(leave);
        btnStatusDatang.setText(arrivalStatue);
        btnStatusPulang.setText(leaveStatue);

        if(!arrivalStatue.equals("On time")){
            btnStatusDatang.setBackground(ContextCompat.getDrawable(view.getContext(),R.drawable.label_red));
        }
        if(!leaveStatue.equals("On time")){
            btnStatusPulang.setBackground(ContextCompat.getDrawable(view.getContext(),R.drawable.label_red));
        }

        if(shift==5) {
            tvstatus.setText("Libur");
        }else{
            tvstatus.setText(statue);
        }
        tvTanggal.setText(getDate(date,view));


        btnUbahStatus = (Button) view.findViewById(R.id.ubah_status_btn);
        btnUbahStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), StatusActivity.class);
                startActivity(intent);
            }
        });
        btnRefresh = (Button) view.findViewById(R.id.refresh_btn);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }
        });





        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    private String getDate(String tanggal, View view){
        String result="";

        String hari = "";
        String bulan = "";


        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();

        int day = today.weekDay;
        int month = today.month;
        int day1 = today.monthDay;
        int tahun = today.year;

        String[] day_record = view.getResources().getStringArray(R.array.hari_record);
        int z =0;
        for (String datax : day_record) {
            try {
                if(day==z){
                    hari=datax;
                    break;
                }
                z++;
            }
            catch (Exception ex){
               // Log.d(TAG, "Error : " + ex.toString());
            }

        }
        String[] month_record = view.getResources().getStringArray(R.array.bulan_record);
        int c =0;
        for (String datax : month_record) {
            try {
                if(month==c){
                    bulan=datax;
                    break;
                }
                c++;
            }
            catch (Exception ex){
                //Log.d(TAG, "Error : " + ex.toString());
            }

        }
        result = hari+", "+day1+" "+bulan+" "+tahun;
        Log.d(TAG, "day : " + result);
        return result;
    }
}
