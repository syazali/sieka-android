package bps.sieka.fragments;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import bps.sieka.R;
import bps.sieka.helpers.Presensi;
import bps.sieka.helpers.PresensiAdapter;
import bps.sieka.helpers.SQLiteHandler;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DashboardFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DashboardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DashboardFragment extends Fragment {
    private static final String TAG = DashboardFragment.class.getSimpleName();
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private PresensiAdapter presensiAdapter;
    private ListView listView;
    private ArrayList<Presensi> presensiArrayList = new ArrayList<>();

    private SQLiteHandler db ;

    public DashboardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DashboardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DashboardFragment newInstance(String param1, String param2) {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        TextView tvTanggalJudul = (TextView) view.findViewById(R.id.tv_tanggal_judul);
        tvTanggalJudul.setText(getDate(view));

        listView = (ListView) view.findViewById(R.id.display_listview);
        SQLiteHandler db = new SQLiteHandler(view.getContext().getApplicationContext());
        String nama, datang, pulang, status,id_shift;
        db.getReadableDatabase();
        Cursor cursor = db.getDashboardData();
        if (cursor.moveToFirst()) {
            do {
                nama = cursor.getString(1);
                datang = cursor.getString(2);
                pulang = cursor.getString(3);
                status = cursor.getString(4);
                id_shift = cursor.getString(5);
                Presensi presensi = new Presensi(nama, datang, pulang, status,id_shift);
                presensiArrayList.add(presensi);
            } while (cursor.moveToNext());
        }
        presensiAdapter = new PresensiAdapter(view.getContext().getApplicationContext(), R.layout.listview_row_layout,presensiArrayList);
        listView.setAdapter(presensiAdapter);


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    private String getDate(View view){
        String result="";

        String hari = "";
        String bulan = "";

        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();

        int day = today.weekDay;
        int month = today.month;
        int day1 = today.monthDay;
        int tahun = today.year;
        String[] day_record = view.getResources().getStringArray(R.array.hari_record);
        int z =0;
        for (String datax : day_record) {
            try {
                if(day==z){
                    hari=datax;
                    break;
                }
                z++;
            }
            catch (Exception ex){
                Log.d(TAG, "Error : " + ex.toString());
            }

        }
        String[] month_record = view.getResources().getStringArray(R.array.bulan_record);
        int c =0;
        for (String datax : month_record) {
            try {
                if(month==c){
                    bulan=datax;
                    break;
                }
                c++;
            }
            catch (Exception ex){
                Log.d(TAG, "Error : " + ex.toString());
            }

        }
        result = hari+", "+day1+" "+bulan+" "+tahun;
        Log.d(TAG, "Day Status  : " + result);
        return result;
    }
}
