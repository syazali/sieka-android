package bps.sieka.apps;

public class AppConfig {
    // Server user login url
    public static String URL_LOGIN = "https://sultradata.com/project/sieka/api/web/index.php/site/login";

    public static String URL_PRESENSI = "https://sultradata.com/project/sieka/api/web/index.php/presensi?access-token=";
    public static String URL_PRESENSI_UPDATE = "https://sultradata.com/project/sieka/api/web/index.php/presensi/update?access-token=";

    // Server user register url
    public static String URL_REGISTER = "http://192.168.0.102/android_login_api/register.php";
}