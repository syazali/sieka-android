package bps.sieka.helpers;

public class Presensi {


    private String nama;
    private String jam_datang;
    private String jam_pulang;
    private String status;

    public String getId_shift() {
        return id_shift;
    }

    public void setId_shift(String id_shift) {
        this.id_shift = id_shift;
    }

    private String id_shift;

    public  Presensi(String nama, String jam_datang, String jam_pulang, String status, String shift){
        this.nama = nama;
        this.jam_datang = jam_datang;
        this.jam_pulang = jam_pulang;
        this.status = status;
        this.id_shift = shift;

    }
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJam_datang() {
        return jam_datang;
    }

    public void setJam_datang(String jam_datang) {
        this.jam_datang = jam_datang;
    }

    public String getJam_pulang() {
        return jam_pulang;
    }

    public void setJam_pulang(String jam_pulang) {
        this.jam_pulang = jam_pulang;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



}
