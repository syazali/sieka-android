package bps.sieka.helpers;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.format.Time;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import bps.sieka.MainActivity;
import bps.sieka.R;
import bps.sieka.apps.AppConfig;
import bps.sieka.apps.AppController;

import static android.content.Context.ALARM_SERVICE;
import static android.content.Context.NOTIFICATION_SERVICE;

public class Notification_receiver extends BroadcastReceiver {
    private boolean mustSync;
    private String msg;
    private boolean errorSync;

    @Override
    public void onReceive(Context context, Intent intent) {
       // Log.e(Notification_receiver.class.getSimpleName(), "Notification_receiver  Start: ");
       // Toast.makeText(context.getApplicationContext(),
           //     "Notification_receiver", Toast.LENGTH_LONG).show();
        try {
            if(intent.getAction().equals("android.intent.action.BOOT_COMPLETED")){
                setNotification(context);
            }
        }catch (NullPointerException ex){
            Log.e(Notification_receiver.class.getSimpleName(), "Notification_receiver  Start: ");
        }

        //kondisi pertama
        //ada internet
        //sync
        //cek

        //kondisi kedua gak ada internet
        //cek

        SQLiteHandler db = new SQLiteHandler(context.getApplicationContext());
        HashMap<String, String> user_detail = db.getUserDetails();
        String access_token = user_detail.get("access_token");
        String id_pegawai = user_detail.get("id");
        syncData(db, db.getDateToday(), id_pegawai, access_token,context);





    }
    private void setNotification(Context context){
        //7.25
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTimeInMillis(System.currentTimeMillis());
        calendar1.set(Calendar.HOUR_OF_DAY,7);
        calendar1.set(Calendar.MINUTE,25);
        calendar1.set(Calendar.SECOND,0);

        ArrayList<Long> alarm_list = new ArrayList<Long>();

        //7.25
        long alarm_1 = calendar1.getTimeInMillis();
        //7.55
        calendar1.add(Calendar.MINUTE,30);
        long alarm_2 = calendar1.getTimeInMillis();

        //15.05
        calendar1.add(Calendar.MINUTE,10+(7*60));
        long alarm_3 = calendar1.getTimeInMillis();

        //15.35
        calendar1.add(Calendar.MINUTE,30);
        long alarm_4 = calendar1.getTimeInMillis();

        //16.05
        calendar1.add(Calendar.MINUTE,30);
        long alarm_5 = calendar1.getTimeInMillis();

        //16.35
        calendar1.add(Calendar.MINUTE,30);
        long alarm_6 = calendar1.getTimeInMillis();

        //17.00
        calendar1.add(Calendar.MINUTE,25);
        long alarm_7 = calendar1.getTimeInMillis();

        alarm_list.add(alarm_1);
        alarm_list.add(alarm_2);
        alarm_list.add(alarm_3);
        alarm_list.add(alarm_4);
        alarm_list.add(alarm_5);
        alarm_list.add(alarm_6);
        alarm_list.add(alarm_7);

        for (int i = 0; i < alarm_list.size(); i++){
            /*Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(alarm_list.get(i));
            Log.e(TAG, "TIME : "+calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE)+":"+calendar.get(Calendar.SECOND));
*/
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);

            Intent intent = new Intent(context, Notification_receiver.class);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(context,i+1,intent,PendingIntent.FLAG_UPDATE_CURRENT);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                // Wakes up the device in Doze Mode
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC, alarm_list.get(i), pendingIntent);
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // Wakes up the device in Idle Mode
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar1.getTimeInMillis(), pendingIntent);
            } else {
                // Old APIs
                alarmManager.set(AlarmManager.RTC, calendar1.getTimeInMillis(), pendingIntent);
            }
        }



    }
    private void cek(String statue, int shift, String arrival, String leave){
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();
        int hour = today.hour;
        int minute = today.minute;
        if(statue.equals("Hadir") && shift!=5 ){
            if(shift==1 || shift ==3){
                if(hour==7 && minute==25 ){
                    if(arrival.equals("null")){

                        mustSync=true;
                        msg = "Belum absensi datang";
                    }
                }else{
                    if(shift==1){
                        if((hour==16 && minute==30) || (hour==16 && minute==5)){
                            if(leave.equals("null")){
                                mustSync=true;
                                msg = "Belum absensi pulang";
                            }

                        }
                    }else{
                        if((hour==17 && minute==0 ) || (hour==16 && minute==35)){
                            if(leave.equals("null")){
                                mustSync=true;
                                msg = "Belum absensi pulang";
                            }

                        }
                    }
                }

            }else{
                //shift 2 ATAU 4
                if(hour==7 && minute==55){
                    if(arrival.equals("null")){
                        mustSync=true;
                        msg = "Belum absensi datang";
                    }

                }else{
                    if(shift==2){

                        if((hour==15 && minute==30 ) || (hour==15 && minute==5) ){
                            if(leave.equals("null")){
                                mustSync=true;
                                msg = "Belum absensi pulang";
                            }

                        }
                    }else{
                        if((hour==17 && minute==0) || (hour==15 && minute==35)){
                            if(leave.equals("null")){
                                mustSync=true;
                                msg = "Belum absensi pulang";
                            }

                        }
                    }
                }
            }
        }
    }
    private void setErrorSync(boolean statue){
        this.errorSync = statue;
    }
    private boolean getErroSync(){
        return errorSync;
    }
    private void syncData(final SQLiteHandler db1,final String tanggal, final String id_pegawai, final String access_token ,final Context context) {
        // Tag used to cancel the request
        String tag_string_req = "req_presensis";

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.URL_PRESENSI+access_token+"&id_pegawai="+id_pegawai+"&tanggal="+tanggal, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session

                        // Now store the user in SQLite
                        SQLiteHandler db = db1;


                        JSONObject todayPresensi = jObj.getJSONObject("presensi");
                        String jamDatang = todayPresensi.getString("jam_datang");
                        String jamPulang = todayPresensi.getString("jam_pulang");
                        String tgl = db.getDateToday();
                        // String idStatus = todayPresensi.getString("id_status");
                        String idStatus = todayPresensi.getString("status");
                        String idShift = todayPresensi.getString("id_shift");

                        // Inserting row in users table
                        db.addPresensi(tgl, jamDatang, jamPulang, idStatus, idShift);


                        JSONArray dashboard_data = jObj.getJSONArray("kantor");
                        JSONObject jsonObject;
                        db.deleteDashboard();
                        for (int i = 0; i < dashboard_data.length(); i++) {
                            jsonObject = dashboard_data.getJSONObject(i);

                            String nama = jsonObject.getString("nama");
                            String jam_datang = jsonObject.getString("jam_datang");
                            String jam_pulang = jsonObject.getString("jam_pulang");
                            String id_status = jsonObject.getString("id_status");
                            String id_shift = jsonObject.getString("id_shift");

                            db.addDashboardData(nama, jam_datang, jam_pulang, id_status, id_shift);


                        }

                        HashMap<String, String> presensi = db.getTodayPresensi();
                        String arrival1 = presensi.get("jam_datang");
                        String leave1 = presensi.get("jam_pulang");
                        int shift1 = Integer.parseInt(presensi.get("id_shift"));
                        String statue1 = presensi.get("id_status");
                        cek(statue1, shift1, arrival1, leave1);

                        if (mustSync) {
                            Intent repeating_intent = new Intent(context, MainActivity.class);
                            PendingIntent contentPendingIntent = PendingIntent.getActivity(context,0,repeating_intent,0);
                            NotificationManager manager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
                            NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                                    .setContentTitle(context.getString(R.string.job_service))
                                    .setContentIntent(contentPendingIntent)
                                    .setSmallIcon(R.mipmap.ic_launcher_sieka_round)
                                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher_sieka_round))
                                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                                    .setDefaults(NotificationCompat.DEFAULT_ALL)
                                    .setAutoCancel(true);
                            builder.setContentText(msg);
                            manager.notify(0, builder.build());
                        }


                        setErrorSync(false);
                        Log.e(Notification_receiver.class.getSimpleName(), "sync berhasil"+getErroSync());

                    }else{
                        Log.e(Notification_receiver.class.getSimpleName(), "Error login");
                    }
                } catch (JSONException e) {
                    // JSON error
                    Log.e(Notification_receiver.class.getSimpleName(), "JSONException error : "+e.getMessage());
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(Notification_receiver.class.getSimpleName(), "VolleyError error : "+error.getMessage());

                HashMap<String, String> presensi = db1.getTodayPresensi();
                String arrival1 = presensi.get("jam_datang");
                String leave1 = presensi.get("jam_pulang");
                int shift1 = Integer.parseInt(presensi.get("id_shift"));
                String statue1 = presensi.get("id_status");
                cek(statue1, shift1, arrival1, leave1);

                if(mustSync){
                    Intent repeating_intent = new Intent(context, MainActivity.class);
                    PendingIntent contentPendingIntent = PendingIntent.getActivity(context,0,repeating_intent,0);
                    NotificationManager manager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                            .setContentTitle(context.getString(R.string.job_service)+" : No internet connection ")
                            .setContentIntent(contentPendingIntent)
                            .setSmallIcon(R.drawable.ic_notifications_active)
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setDefaults(NotificationCompat.DEFAULT_ALL)
                            .setContentText("Sistem tidak bisa melakukan pengecekan dikarenakan kendala jaringan internet, pastikan smartphone anda terhubung dengan jaringan internet atau pastikan anda telah melakukan presensi")
                            .setStyle(new NotificationCompat.BigTextStyle()
                                    .bigText("Sistem tidak bisa melakukan pengecekan dikarenakan kendala jaringan internet, pastikan smartphone anda terhubung dengan jaringan internet atau pastikan anda telah melakukan presensi"))
                            .setAutoCancel(true);


                    manager.notify(0, builder.build());
                }
            }
        }) {

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }
}
