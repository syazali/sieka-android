package bps.sieka.helpers;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.widget.ListView;

import bps.sieka.R;

public class BackgroundTask extends AsyncTask<String,Presensi,String> {

    Context ctx;
    PresensiAdapter presensiAdapter;
    Activity activity;
    ListView listView;
    public BackgroundTask(Context context){
        this.ctx =context;
        this.activity = (Activity) context;
    }
    @Override
    protected String doInBackground(String... params) {
        listView = activity.findViewById(R.id.display_listview);
        SQLiteHandler db = new SQLiteHandler(ctx);
        String method = params[0];
        String nama, datang, pulang, status;
        if (method.equals("retrive")){
                db.getReadableDatabase();
            Cursor cursor = db.getDashboardData();
           // presensiAdapter = new PresensiAdapter(ctx, R.layout.listview_row_layout);

            while (cursor.moveToNext()){
                nama = cursor.getString(1);
                datang = cursor.getString(2);
                pulang = cursor.getString(3);
                status = cursor.getString(4);
              //  Presensi presensi = new Presensi(nama, datang, pulang, status);
              //  publishProgress(presensi);

            }
            return "retrive";
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Presensi... values) {
        presensiAdapter.add(values[0]);
    }

    @Override
    protected void onPostExecute(String result) {
        if(result.equals("retrive"))
        {
            listView.setAdapter(presensiAdapter);
        }else{

        }

    }
}
