package bps.sieka.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import bps.sieka.R;

public class SQLiteHandler extends SQLiteOpenHelper {

    private static final String TAG = SQLiteHandler.class.getSimpleName();
    private final Context fcontext;

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "android_api";

    // Login table name
    private static final String TABLE_USER = "user";
    private static final String TABLE_PRESENSI = "presensi";
    private static final String TABLE_STATUS = "status";
    private static final String TABLE_SHIFT = "shift";
    private static final String TABLE_DASHBOARD = "dashboard";

    // Login Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";;
    private static final String KEY_ACCESS_TOKEN = "access_token";

    private static final String PRESENSI_ID = "id";
    private static final String PRESENSI_TANGGAL = "tanggal";
    private static final String PRESENSI_JAM_DATANG = "jam_datang";
    private static final String PRESENSI_JAM_PULANG = "jam_pulang";
    private static final String PRESENSI_ID_STATUS = "id_status";
    private static final String PRESENSI_ID_SHIFT = "id_shift";

    private static final String DASHBOARD_ID = "id";
    private static final String DASHBOARD_NAMA = "nama";
    private static final String DASHBOARD_JAM_DATANG = "jam_datang";
    private static final String DASHBOARD_JAM_PULANG = "jam_pulang";
    private static final String DASHBOARD_ID_STATUS = "id_status";
    private static final String DASHBOARD_ID_SHIFT = "id_shift";

    private static final String STATUS_ID = "id";
    private static final String STATUS_NAMA = "nm_status";

    private static final String SHIFT_ID = "id";
    private static final String SHIFT_NAMA = "nm_shift";


    public SQLiteHandler(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        fcontext = context;
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_USER + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_ACCESS_TOKEN + " TEXT" + ")";

        String CREATE_PRESENSI_TABLE = "CREATE TABLE " + TABLE_PRESENSI + "("
                + PRESENSI_ID + " INTEGER PRIMARY KEY," + PRESENSI_TANGGAL + " TEXT,"
                + PRESENSI_JAM_DATANG + " TEXT," + PRESENSI_JAM_PULANG + " TEXT,"
                + PRESENSI_ID_STATUS+ " TEXT,"
                + PRESENSI_ID_SHIFT + " TEXT" + ")";

        String CREATE_STATUS_TABLE = "CREATE TABLE " + TABLE_STATUS + "("
                + STATUS_ID + " INTEGER PRIMARY KEY," + STATUS_NAMA + " TEXT" + ")";

        String CREATE_SHIFT_TABLE = "CREATE TABLE " + TABLE_SHIFT + "("
                + SHIFT_ID + " INTEGER PRIMARY KEY," + SHIFT_NAMA + " TEXT" + ")";

        String CREATE_DASHBOARD_TABLE = "CREATE TABLE " + TABLE_DASHBOARD + "("
                + DASHBOARD_ID + " INTEGER PRIMARY KEY," + DASHBOARD_NAMA + " TEXT,"
                + DASHBOARD_JAM_DATANG + " TEXT," + DASHBOARD_JAM_PULANG + " TEXT,"
                + DASHBOARD_ID_STATUS+ " TEXT,"
                + DASHBOARD_ID_SHIFT + " TEXT" + ")";

        db.execSQL(CREATE_LOGIN_TABLE);
        db.execSQL(CREATE_PRESENSI_TABLE);
        db.execSQL(CREATE_STATUS_TABLE);
        db.execSQL(CREATE_SHIFT_TABLE);
        db.execSQL(CREATE_DASHBOARD_TABLE);

        Log.d(TAG, "4 Database tables created");

        ContentValues values = new ContentValues();
        Resources res = fcontext.getResources();
        String[] _mytable_Records = res.getStringArray(R.array.status_record);
        for (String datax : _mytable_Records) {
            values.put(STATUS_NAMA, datax); // Name
            db.insert(TABLE_STATUS, null, values);
        }
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STATUS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRESENSI);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHIFT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DASHBOARD);

        // Create tables again
        onCreate(db);
    }


    /**
     * Storing user details in database
     * */
    public void addUser(String idPeg,String name, String access_token) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, idPeg); // Name
        values.put(KEY_NAME, name); // Name
        values.put(KEY_ACCESS_TOKEN, access_token); // Email

        // Inserting Row
        long id = db.insert(TABLE_USER, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New user inserted into sqlite: " + id);
    }
    public void addPresensi(String tanggal, String jam_datang, String jam_pulang, String id_status, String id_shift) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PRESENSI, null, null);

        ContentValues values = new ContentValues();

        values.put(PRESENSI_TANGGAL, tanggal); // Name
        values.put(PRESENSI_JAM_DATANG, jam_datang); // Email
        values.put(PRESENSI_JAM_PULANG, jam_pulang); // Email
        values.put(PRESENSI_ID_STATUS, id_status); // Created At
        values.put(PRESENSI_ID_SHIFT, id_shift); // Created At

        // Inserting Row
        long id = db.insert(TABLE_PRESENSI, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New presensi inserted into sqlite: " + id);
    }
    public void addDashboardData(String nama, String jam_datang, String jam_pulang, String id_status, String id_shift) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DASHBOARD_NAMA, nama); // Name
        values.put(DASHBOARD_JAM_DATANG, jam_datang); // Email
        values.put(DASHBOARD_JAM_PULANG, jam_pulang); // Email
        values.put(DASHBOARD_ID_STATUS, id_status); // Created At
        values.put(DASHBOARD_ID_SHIFT, id_shift); // Created At

        // Inserting Row
        long id = db.insert(TABLE_DASHBOARD, null, values);
        db.close(); // Closing database connection

        Log.e(TAG, "New data dashboard inserted into sqlite: " + id);
    }

    /**
     * Getting user data from database
     * */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_USER;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            user.put("id", cursor.getString(0));
            user.put("name", cursor.getString(1));
            user.put("access_token", cursor.getString(2));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + user.toString());

        return user;
    }
    public HashMap<String, String> getTodayPresensi() {
        HashMap<String, String> presensi = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_PRESENSI;
        Log.d(TAG, "Query: " + selectQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            presensi.put("tanggal", cursor.getString(1));
            presensi.put("jam_datang", cursor.getString(2));
            presensi.put("jam_pulang", cursor.getString(3));
            presensi.put("id_status", cursor.getString(4));
            presensi.put("id_shift", cursor.getString(5));
        }
        Log.d(TAG, "Query: " + cursor.getCount());
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching presensi from Sqlite: " + presensi.toString());

        return presensi;
    }

    /**
     * Re crate database Delete all tables and create them again
     * */
    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_USER, null, null);
        db.close();
        Log.d(TAG, "Deleted all user info from sqlite");
    }
    public void deleteDashboard() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_DASHBOARD, null, null);
        db.close();
        Log.d(TAG, "Deleted data dashboard info from sqlite");
    }
    public void deletePresensiToday() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_PRESENSI, null, null);
        db.close();
        Log.d(TAG, "Deleted Presensi today from sqlite");
    }
    public String getDateToday() {
        Locale id = new Locale("in", "ID");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", id);
        Date date = new Date();
        return dateFormat.format(date);

    }
    public  Cursor getDashboardData(){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_DASHBOARD;

        return db.rawQuery(selectQuery, null);
    }

}
