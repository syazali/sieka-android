package bps.sieka.helpers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import bps.sieka.R;

public class PresensiAdapter extends ArrayAdapter<Presensi> {

    private static final String TAG = "FruitArrayAdapter";
    private List<Presensi> presensiList = new ArrayList<Presensi>();
    private Context context;
    private Locale id = new Locale("in", "ID");

    public PresensiAdapter(Context context, int textViewResourceId,ArrayList<Presensi> objects) {
        super(context, textViewResourceId, objects);

        this.context = context;
        this.presensiList = objects;

    }
    static class PresensiHolder{
        TextView tv_nama, tv_datang, tv_pulang, tv_status;
    }
    @Override
    public void add(Presensi object) {
        presensiList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.presensiList.size();
    }

    @Override
    public Presensi getItem(int index) {
        return this.presensiList.get(index);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        PresensiHolder viewHolder;


        Presensi presensi = getItem(position);

            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if(presensi.getStatus().equals("Handkey tidak lengkap")){
                row = inflater.inflate(R.layout.listview_row_layout_warning, parent, false);
            }
            else if(presensi.getStatus().equals("Alpha")){
                row = inflater.inflate(R.layout.listview_row_layout_alpha, parent, false);
            }
            else if(presensi.getStatus().equals("Terlambat")){
                row = inflater.inflate(R.layout.listview_row_layout_error, parent, false);
            }
            else{
                row = inflater.inflate(R.layout.listview_row_layout, parent, false);
            }

            viewHolder = new PresensiHolder();
            viewHolder.tv_nama = (TextView) row.findViewById(R.id.nama);
            viewHolder.tv_datang = (TextView) row.findViewById(R.id.jam_datang);
            viewHolder.tv_pulang = (TextView) row.findViewById(R.id.jam_pulang);
            viewHolder.tv_status = (TextView) row.findViewById(R.id.status);
            row.setTag(viewHolder);


        viewHolder.tv_nama.setText(presensi.getNama());
        viewHolder.tv_datang.setText(presensi.getJam_datang());
        viewHolder.tv_pulang.setText(presensi.getJam_pulang());
        viewHolder.tv_status.setText(presensi.getStatus());
        return row;
    }




}
