package bps.sieka;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import bps.sieka.apps.AppConfig;
import bps.sieka.apps.AppController;
import bps.sieka.helpers.SQLiteHandler;
import bps.sieka.helpers.SessionManager;

public class StatusActivity extends AppCompatActivity {
    private static final String TAG ="StatusActivity" ;
    private SQLiteHandler db;
    private SessionManager session;
    private ProgressDialog pDialog;
    private Button backNavBtn;
    private Button updateBtn;
    private TextView tvTanggalJudul;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);
        backNavBtn = (Button) findViewById(R.id.back_btn);
        updateBtn = (Button) findViewById(R.id.update_btn);
        db = new SQLiteHandler(getApplicationContext());

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        tvTanggalJudul = (TextView) findViewById(R.id.tv_update_tanggal_judul);
        tvTanggalJudul.setText(getDate());

        backNavBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                // Intent intent = new Intent(StatusActivity.this, MainActivity.class);
                // startActivity(intent);
            }
        });
        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> user = db.getUserDetails();

                String access_token = user.get("access_token");
                String id_pegawai = user.get("id");

                Spinner spinner = (Spinner) findViewById(R.id.spinner_update_status);
                int id_status = spinner.getSelectedItemPosition();
                if(id_status>0){
                    id_status = id_status+2;
                }
                else{
                    id_status = id_status+1;
                }
                updateData(db.getDateToday(),id_pegawai,access_token,String.valueOf(id_status));
            }
        });
    }
    private void updateData(final String tanggal, final String id_pegawai, final String access_token, final String status) {
        // Tag used to cancel the request
        String tag_string_req = "update_presensi";
        Log.e(TAG, "Update: ");
        pDialog.setMessage("Update Status.....");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.PUT,
                AppConfig.URL_PRESENSI_UPDATE+access_token+"&id_pegawai="+id_pegawai+"&tanggal="+tanggal+"&status="+status, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session

                        finish();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        "Error : Tidak ada koneksi internet", Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private String getDate(){
        String result="";

        String hari = "";
        String bulan = "";

        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();

        int day = today.weekDay;
        int month = today.month;
        int day1 = today.monthDay;
        int tahun = today.year;
        String[] day_record = getResources().getStringArray(R.array.hari_record);
        int z =0;
        for (String datax : day_record) {
            try {
                if(day==z){
                    hari=datax;
                    break;
                }
                z++;
            }
            catch (Exception ex){
                Log.e(TAG, "Error : " + ex.toString());
            }

        }
        String[] month_record = getResources().getStringArray(R.array.bulan_record);
        int c =0;
        for (String datax : month_record) {
            try {
                if(month==c){
                    bulan=datax;
                    break;
                }
                c++;
            }
            catch (Exception ex){
                Log.e(TAG, "Error : " + ex.toString());
            }

        }
        result = hari+", "+day1+" "+bulan+" "+tahun;
        Log.e(TAG, "day : " + result);
        return result;
    }
}
